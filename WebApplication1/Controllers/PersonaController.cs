﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{   
    [RoutePrefix("api/Persona")]
    public class PersonaController : ApiController
    {
        [HttpPost]
        [ActionName("Add")]

        public IHttpActionResult Add(Models.ViewModel.PersonaRequest model)
        {

            using (Models.pruebillaEntities1 db = new Models.pruebillaEntities1())
            {
                var oPersona = new Models.Persona();
                oPersona.nombre = model.Nombre;
                oPersona.edad = model.Edad;
                db.Persona.Add(oPersona);
                db.SaveChanges();
            }

            return Ok("Realizado");
        }
    }
}
